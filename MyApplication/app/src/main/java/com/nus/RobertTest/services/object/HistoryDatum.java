package com.nus.RobertTest.services.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HistoryDatum {

    @SerializedName("CurrencyID")
    @Expose
    private Integer currencyID;
    @SerializedName("Price")
    @Expose
    private Integer price;
    @SerializedName("PriceChangeDate")
    @Expose
    private String priceChangeDate;

    public Integer getCurrencyID() {
        return currencyID;
    }

    public Integer getPrice() {
        return price;
    }

    public String getPriceChangeDate() {
        return priceChangeDate;
    }
}
