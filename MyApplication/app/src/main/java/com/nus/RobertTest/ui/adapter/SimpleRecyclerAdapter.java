package com.nus.RobertTest.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nus.RobertTest.R;
import com.nus.RobertTest.events.ButtonMapClickEvent;
import com.nus.RobertTest.events.ButtonStreetMapClickEvent;
import com.nus.RobertTest.services.object.ApiObject;

import org.greenrobot.eventbus.EventBus;

public class SimpleRecyclerAdapter extends RecyclerView.Adapter<SimpleRecyclerAdapter.VersionViewHolder> {

    private ApiObject objDetail;

    public SimpleRecyclerAdapter(ApiObject obj) {
        this.objDetail = obj;

    }

    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view;
        if (i == 0) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerlist_item, viewGroup, false);
        }else if (i == 1){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_item_list, viewGroup, false);
        }else if (i == 2){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_item_list_detail, viewGroup, false);
        }else if (i == 3){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_item_list_info, viewGroup, false);
        }else if (i == 4 || i == 5){
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_item_list_standard, viewGroup, false);
        }else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_item_list_bottom, viewGroup, false);
        }

        VersionViewHolder viewHolder = new VersionViewHolder(view,i);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VersionViewHolder versionViewHolder, int i) {
        if (i == 0) {
            versionViewHolder.txtCHF.setText("CHF " + objDetail.getPriceValue().toString());
            versionViewHolder.txtTitle.setText(objDetail.getStreetName());
            versionViewHolder.txtZip.setText(objDetail.getZip());
            versionViewHolder.txtCity.setText(objDetail.getCity());
        }else if (i == 1){
            versionViewHolder.txtName.setText(objDetail.getTitle());
            versionViewHolder.txtDesc.setText(objDetail.getDescription());
        }else if (i == 2){

            versionViewHolder.txtProperty.setText(objDetail.getPropertyTypeString());
            versionViewHolder.txtMonth1.setText("CHF " + objDetail.getPriceValue().toString());
            if (objDetail.getExtraCosts() == null) {
                versionViewHolder.itemMonth2.setLayoutParams(new LinearLayout.LayoutParams(0,0));
                versionViewHolder.itemCharge.setLayoutParams(new LinearLayout.LayoutParams(0,0));
            }else {
                int month = objDetail.getPriceValue() - objDetail.getExtraCosts();
                versionViewHolder.txtMonth2.setText("CHF " + month);
                versionViewHolder.txtCharges.setText("CHF " + objDetail.getExtraCosts().toString());
            }

            if (objDetail.getLivingSpace() == null) {
                versionViewHolder.itemLiving.setLayoutParams(new LinearLayout.LayoutParams(0,0));
            }else  {
                versionViewHolder.txtLivingSpaces.setText(objDetail.getLivingSpace().toString() + " m²");
            }

            if (objDetail.getUsefulArea() == null) {
                versionViewHolder.itemFloorSpace.setLayoutParams(new LinearLayout.LayoutParams(0,0));
            }else {
                versionViewHolder.txtFloorSpaces.setText(objDetail.getUsefulArea() + " m²");
            }

            if (objDetail.getRooms() == null) {
                versionViewHolder.itemRoom.setLayoutParams(new LinearLayout.LayoutParams(0,0));
            }else {
                versionViewHolder.txtRooms.setText(objDetail.getRooms().toString());
            }
            if (objDetail.getFloor() == null) {
                versionViewHolder.itemFloor.setLayoutParams(new LinearLayout.LayoutParams(0,0));
            }else {
                versionViewHolder.txtFloor.setText(objDetail.getFloor().toString());
            }
            if (objDetail.getYearOfConstruction() == null) {
                versionViewHolder.itemYear.setLayoutParams(new LinearLayout.LayoutParams(0,0));
            }else {
                versionViewHolder.txtYears.setText(objDetail.getYearOfConstruction().toString());
            }

            if (objDetail.getAvailability() == null) {
                versionViewHolder.itemAvailable.setLayoutParams(new LinearLayout.LayoutParams(0,0));
            }else {
                versionViewHolder.txtAvailable.setText(objDetail.getAvailability().toString());
            }

        } else if (i == 3){
            if (objDetail.getLocationFactors() == null){
                versionViewHolder.txtSun.setText("");
                versionViewHolder.txtAlt.setText("");
            } else {
                versionViewHolder.txtAlt.setText(objDetail.getLocationFactors().getHeight() + " m. above sea level");
                versionViewHolder.txtSun.setText(objDetail.getLocationFactors().getSunshineHoursSummer() + "h (summer)\n" +
                                                 objDetail.getLocationFactors().getSunshineHoursWinter() + "h (winter)");
            }
        } else if (i == 4) {
            versionViewHolder.txtTitleStandard.setText(R.string.str_title_standard);
            if (objDetail.getFeatures() == null) {
                versionViewHolder.txtStandard.setText("");
            }else {
                String str = objDetail.getFeatures();
                str = str.replace(", ","\n\n");
                versionViewHolder.txtStandard.setText(str);
            }
        } else if (i == 5) {
            versionViewHolder.txtTitleStandard.setText(R.string.str_title_nearby);
            if (objDetail.getDistances() == null) {
                versionViewHolder.txtStandard.setText("");
            }else {
                String str = objDetail.getDistances();
                str = str.replace(", ","\n\n");
                versionViewHolder.txtStandard.setText(str);
            }
        } else {
            if (objDetail.getContact().contactName == null) {
                versionViewHolder.txtNameContact.setText("N/A");
            }else {
                versionViewHolder.txtNameContact.setText(objDetail.getContact().contactName.toString());
            }

            if (objDetail.getContact().vendorContact == null) {
                versionViewHolder.txtVendorContact.setText("N/A");
            }else {
                versionViewHolder.txtVendorContact.setText(objDetail.getContact().vendorContact.toString());
            }

            if (objDetail.getContact().contactTel == null) {
                versionViewHolder.txtTelContact.setText("N/A");
            }else {
                versionViewHolder.txtTelContact.setText(objDetail.getContact().contactTel.toString());
            }
        }
    }

    @Override
    public int getItemCount() {
        return 7;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    class VersionViewHolder extends RecyclerView.ViewHolder {
        LinearLayout itemLayout;
        LinearLayout itemDetail;
        LinearLayout itemMoreDetail;
        LinearLayout itemLiving;
        LinearLayout itemFloor;
        LinearLayout itemFloorSpace;
        LinearLayout itemMonth2;
        LinearLayout itemRoom;
        LinearLayout itemAvailable;
        LinearLayout itemCharge;
        LinearLayout itemYear;
        LinearLayout itemInfo;
        LinearLayout itemStandard;
        LinearLayout itemBottom;
        TextView txtCHF;
        TextView txtTitle;
        TextView txtZip;
        TextView txtCity;
        TextView txtName;
        TextView txtDesc;
        TextView txtProperty;
        TextView txtMonth1;
        TextView txtMonth2;
        TextView txtCharges;
        TextView txtLivingSpaces;
        TextView txtFloorSpaces;
        TextView txtRooms;
        TextView txtFloor;
        TextView txtYears;
        TextView txtAvailable;
        TextView txtAlt;
        TextView txtSun;
        TextView txtStandard;
        TextView txtTitleStandard;
        TextView txtNameContact;
        TextView txtVendorContact;
        TextView txtTelContact;
        ImageButton btnMap;
        ImageButton btnStreet;

        public VersionViewHolder(View itemView, int index) {
            super(itemView);

            if (index == 0){
                itemLayout = (LinearLayout) itemView.findViewById(R.id.list_item);
                txtCHF = (TextView) itemView.findViewById(R.id.text_chf);
                txtTitle = (TextView) itemView.findViewById(R.id.list_item_name);
                txtZip = (TextView) itemView.findViewById(R.id.text_zip);
                txtCity = (TextView) itemView.findViewById(R.id.text_city);
                btnMap = (ImageButton) itemView.findViewById(R.id.button_map);
                btnMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().postSticky(new ButtonMapClickEvent(btnMap));
                    }
                });
                btnStreet = (ImageButton)itemView.findViewById(R.id.button_street);
                btnStreet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EventBus.getDefault().postSticky(new ButtonStreetMapClickEvent(btnStreet));
                    }
                });
            } else if (index == 1){
                itemDetail = (LinearLayout) itemView.findViewById(R.id.list_item_detail);
                txtName = (TextView) itemView.findViewById(R.id.text_name);
                txtDesc = (TextView) itemView.findViewById(R.id.text_desc);
            } else if (index == 2){
                itemLiving = (LinearLayout) itemView.findViewById(R.id.linear_living);
                itemFloor = (LinearLayout) itemView.findViewById(R.id.linear_floor);
                itemFloorSpace = (LinearLayout) itemView.findViewById(R.id.linear_floor_space);
                itemAvailable = (LinearLayout) itemView.findViewById(R.id.linear_available);
                itemRoom = (LinearLayout) itemView.findViewById(R.id.linear_rooms);
                itemCharge = (LinearLayout) itemView.findViewById(R.id.linear_charge);
                itemMoreDetail = (LinearLayout) itemView.findViewById(R.id.list_item_more_detail);
                itemMonth2 = (LinearLayout) itemView.findViewById(R.id.linear_month_2);
                itemYear = (LinearLayout) itemView.findViewById(R.id.linear_year);
                txtProperty = (TextView) itemView.findViewById(R.id.text_property);
                txtMonth1 = (TextView) itemView.findViewById(R.id.text_month_1);
                txtMonth2 = (TextView) itemView.findViewById(R.id.text_month_2);
                txtCharges = (TextView) itemView.findViewById(R.id.text_charges);
                txtLivingSpaces = (TextView) itemView.findViewById(R.id.text_living_space);
                txtFloorSpaces = (TextView) itemView.findViewById(R.id.text_floor_space);
                txtRooms = (TextView) itemView.findViewById(R.id.text_room);
                txtFloor = (TextView) itemView.findViewById(R.id.text_floor);
                txtYears = (TextView) itemView.findViewById(R.id.text_year);
                txtAvailable = (TextView) itemView.findViewById(R.id.text_available);
            } else if (index == 3){
                itemInfo = (LinearLayout) itemView.findViewById(R.id.list_item_info);
                txtAlt = (TextView) itemView.findViewById(R.id.text_altitude);
                txtSun = (TextView) itemView.findViewById(R.id.text_sunshines);
            } else if (index == 4 || index == 5) {
                itemStandard = (LinearLayout) itemView.findViewById(R.id.list_item_standard);
                txtStandard = (TextView) itemView.findViewById(R.id.text_standard);
                txtTitleStandard = (TextView) itemView.findViewById(R.id.text_title_standard);
            } else {
                itemBottom = (LinearLayout) itemView.findViewById(R.id.list_item_bottom);
                txtNameContact = (TextView) itemView.findViewById(R.id.text_contact_name);
                txtVendorContact = (TextView) itemView.findViewById(R.id.text_vendor_contact);
                txtTelContact = (TextView) itemView.findViewById(R.id.text_tel);
            }
        }
    }
}
