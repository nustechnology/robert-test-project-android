package com.nus.RobertTest.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.nus.RobertTest.R;

public class LoadingViewUtil {

    private static ProgressBar mProgressBar;
    private static ProgressDialog mProgressDialog;
    private boolean isShowing;
    private Context mContext;

    public boolean isShowing() {
        return isShowing;
    }

    public LoadingViewUtil(Context context) {
        mContext = context;
        setupProgressBar(context);
    }

    public void show(boolean isLock) {
        if (mProgressBar == null) {
            new LoadingViewUtil(mContext);
        }

        if (isLock) {
            if (!isShowing()) {
                setupProgressDialog(mContext);
                isShowing = true;
            }
        } else {
            mProgressBar.setVisibility(View.VISIBLE);
            isShowing = true;
        }
    }

    public void hide() {
        try {
            if (mProgressBar != null) {
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
            isShowing = false;
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void setupProgressBar(Context context) {
        mProgressBar = new ProgressBar(context, null, android.R.attr.progressBarStyle);
        mProgressBar.setIndeterminate(true);
        mProgressBar.setVisibility(View.INVISIBLE);
        ViewGroup layout = (ViewGroup) ((Activity) context).findViewById(android.R.id.content).getRootView();
        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        RelativeLayout rl = new RelativeLayout(context);
        rl.setGravity(Gravity.CENTER);
        rl.addView(mProgressBar);
        layout.addView(rl, params);
    }

    private static void setupProgressDialog(Context context) {
        mProgressDialog = new ProgressDialog(context, R.style.LoadingDialog);
        mProgressDialog.show();
        mProgressDialog.setContentView(new ProgressBar(context));
        mProgressDialog.setCancelable(false);
    }
}
