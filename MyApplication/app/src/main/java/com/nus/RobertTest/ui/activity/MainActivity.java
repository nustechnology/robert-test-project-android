package com.nus.RobertTest.ui.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nus.RobertTest.R;
import com.nus.RobertTest.events.ButtonMapClickEvent;
import com.nus.RobertTest.events.ButtonStreetMapClickEvent;
import com.nus.RobertTest.services.ServiceGenerator;
import com.nus.RobertTest.services.object.ApiObject;
import com.nus.RobertTest.ui.adapter.SimpleRecyclerAdapter;
import com.nus.RobertTest.utils.PermissionsUtil;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener, OnStreetViewPanoramaReadyCallback, OnMapReadyCallback {

    private int appId = 14823955;

    SimpleRecyclerAdapter simpleRecyclerAdapter;
    private ApiObject object;
    private List<String> arrUrl;

    @ViewById(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @ViewById(R.id.scrollableview)
    RecyclerView recyclerView;

    @ViewById(R.id.slider)
    SliderLayout sliderShow;

    @ViewById(R.id.button_zoom)
    Button btnZoom;

    //map
    private boolean isShowingMap;
    private boolean isFullScreen;
    private MapFragment mapFragment;
    private StreetViewPanoramaFragment mapStreetFragment;
    private FrameLayout mapLayout, mapStreetLayout;
    private AppBarLayout mainLayout;
    private GoogleMap googleMap;
    private StreetViewPanorama streetViewPanorama;
    private int mapType;
    private Point screenSize;
    private LatLng TutorialsPoint = new LatLng(43.1, -87.9);

    private ImageButton btnMap, btnStreetMap;

    @Override
    protected void init() {
        super.init();

        Display display = getWindowManager().getDefaultDisplay();
        screenSize = new Point();
        display.getSize(screenSize);
        sliderShow.stopAutoCycle();
        if (Build.VERSION.SDK_INT >= 23) {
            checkPermission();
        } else {
            callApi(appId);
            setupMapView();
        }
    }

    private JSONObject getJson(int appId) {
        try {

            JSONObject jsonHeader = new JSONObject();
            jsonHeader.put("ApplicationVersion", null);
            jsonHeader.put("BundleName", null);
            jsonHeader.put("Device", null);
            jsonHeader.put("DeviceApplicationGUID", "00000000-0000-0000-0000-000000000000");
            jsonHeader.put("Language", "de");
            jsonHeader.put("Model", null);
            jsonHeader.put("RequestKey", 0);
            jsonHeader.put("ScreenHeight", 0);
            jsonHeader.put("ScreenWidth", 0);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("AdId", appId);
            jsonObject.put("Header", jsonHeader);
            return jsonObject;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }

    private void checkPermission() {

        if (!PermissionsUtil.checkPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)) {

            PermissionsUtil.requestPermissions(this, 236, Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION);
        } else {
            callApi(appId);
            setupMapView();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean b = true;
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                b = false;
                break;
            }
        }
        if (b) {
            callApi(appId);
            setupMapView();
        } else {
            finish();
        }
    }

    @Override
    protected boolean useEventBus() {
        return true;
    }

    @Click(R.id.button_zoom)
    void onClick() {

        if (!isFullScreen) {
            if (Build.VERSION.SDK_INT >= 21)
                mainLayout.getLayoutParams().height = screenSize.y;
            else
                mainLayout.getLayoutParams().height = screenSize.y - (int) convertDpToPixel(20);

            btnZoom.setBackgroundResource(R.drawable.ic_chevron_up);
            isFullScreen = true;
        } else {
            btnZoom.setBackgroundResource(R.drawable.ic_chevron_down);
            mainLayout.getLayoutParams().height = (int) convertDpToPixel(280);
            isFullScreen = false;
        }
        mainLayout.requestLayout();
    }

    @Subscribe
    public void onEvent(ButtonMapClickEvent event) {

        btnMap = event.getBtn();
        if (!isShowingMap) {//show map
            sliderShow.setVisibility(View.INVISIBLE);
            mapStreetLayout.setVisibility(View.INVISIBLE);
            mapLayout.setVisibility(View.VISIBLE);
            btnZoom.setVisibility(View.VISIBLE);
            mapType = 1;
            isShowingMap = true;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(TutorialsPoint, 15);
            googleMap.animateCamera(cameraUpdate);
            googleMap.addMarker(new MarkerOptions()
                    .position(TutorialsPoint)
                    .title("Marker"));
        } else {
            if (mapType == 1) {
                sliderShow.setVisibility(View.VISIBLE);
                mapLayout.setVisibility(View.INVISIBLE);
                mapStreetLayout.setVisibility(View.INVISIBLE);
                btnZoom.setVisibility(View.INVISIBLE);
                isShowingMap = false;
            } else {
                isShowingMap = false;
                onEvent(new ButtonMapClickEvent(btnMap));
            }
        }

        showImage();
    }

    @Subscribe
    public void onEvent(ButtonStreetMapClickEvent event) {

        btnStreetMap = event.getBtn();
        if (!isShowingMap) {//show map
            sliderShow.setVisibility(View.INVISIBLE);
            mapLayout.setVisibility(View.INVISIBLE);
            mapStreetLayout.setVisibility(View.VISIBLE);
            mapType = 2;
            isShowingMap = true;
            btnZoom.setVisibility(View.VISIBLE);
            streetViewPanorama.setPosition(TutorialsPoint);
        } else {
            if (mapType == 2) {
                sliderShow.setVisibility(View.VISIBLE);
                mapLayout.setVisibility(View.INVISIBLE);
                mapStreetLayout.setVisibility(View.INVISIBLE);
                btnZoom.setVisibility(View.INVISIBLE);
                isShowingMap = false;
            } else {
                isShowingMap = false;
                onEvent(new ButtonStreetMapClickEvent(btnStreetMap));
            }
        }

        showImage();
    }

    private void showImage(){

        if (isShowingMap){
            if (mapType == 1){
                btnMap.setBackgroundResource(R.drawable.ic_images);
                btnStreetMap.setBackgroundResource(R.drawable.ic_street_view);
            }
            else {
                btnMap.setBackgroundResource(R.drawable.ic_map);
                btnStreetMap.setBackgroundResource(R.drawable.ic_images);
            }
        }
        else {
            btnMap.setBackgroundResource(R.drawable.ic_map);
            btnStreetMap.setBackgroundResource(R.drawable.ic_street_view);
        }
    }

    public float convertDpToPixel(float dp) {
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    private void setupMapView() {

        mainLayout = (AppBarLayout) findViewById(R.id.appbar);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapview);
        mapLayout = (FrameLayout) findViewById(R.id.mapview);
        if (mapLayout != null)
            mapLayout.setVisibility(View.INVISIBLE);

        mapFragment.getMapAsync(this);
        MapsInitializer.initialize(this);

        mapStreetFragment = (StreetViewPanoramaFragment) getFragmentManager().findFragmentById(R.id.map_street_view);
        mapStreetLayout = (FrameLayout) findViewById(R.id.map_street_view);
        if (mapStreetLayout != null)
            mapStreetLayout.setVisibility(View.INVISIBLE);

        mapStreetFragment.getStreetViewPanoramaAsync(this);
        btnZoom.setVisibility(View.INVISIBLE);

        CoordinatorLayout.LayoutParams par =
                (CoordinatorLayout.LayoutParams) mainLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = new AppBarLayout.Behavior();
        behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
            @Override
            public boolean canDrag(AppBarLayout appBarLayout) {
                return false;
            }
        });
        par.setBehavior(behavior);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {

        this.streetViewPanorama = streetViewPanorama;
    }

    private void callApi(int stringId) {

        JSONObject json = getJson(stringId);
        if (json!= null) {
            showLoading(true);
            Call<ApiObject> call = ServiceGenerator.getService().testApi(json);
            call.enqueue(new Callback<ApiObject>() {
                @Override
                public void onResponse(Call<ApiObject> call, Response<ApiObject> response) {

                    if (response != null && response.body() != null) {
                        MainActivity.this.object = response.body();
                        arrUrl = new ArrayList<String>();
                        arrUrl = object.getImageUrls();

                        if (arrUrl != null) {
                            for (String name : arrUrl) {
                                DefaultSliderView defaultSliderView = new DefaultSliderView(MainActivity.this);
                                // initialize a SliderLayout
                                defaultSliderView
                                        .image(name)
                                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                                        .setOnSliderClickListener(MainActivity.this);
                                //add your extra information
                                defaultSliderView.bundle(new Bundle());
                                defaultSliderView.getBundle()
                                        .putString("extra", name);
                                sliderShow.addSlider(defaultSliderView);

                                if (MainActivity.this.object.getGeoCoordinates() != null) {
                                    float latitude = MainActivity.this.object.getGeoCoordinates().getLatitude();
                                    float longitude = MainActivity.this.object.getGeoCoordinates().getLongitude();
                                    MainActivity.this.TutorialsPoint = new LatLng(latitude, longitude);
                                }
                            }
                            sliderShow.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderShow.setCustomAnimation(new DescriptionAnimation());
                            sliderShow.setDuration(4000000);
                            sliderShow.addOnPageChangeListener(MainActivity.this);
                        }

                        recyclerView.setHasFixedSize(true);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                        recyclerView.setLayoutManager(linearLayoutManager);

                        if (simpleRecyclerAdapter == null) {
                            simpleRecyclerAdapter = new SimpleRecyclerAdapter(MainActivity.this.object);
                            recyclerView.setAdapter(simpleRecyclerAdapter);
                        }
                    }

                    MainActivity.this.showLoading(false);
                }

                @Override
                public void onFailure(Call<ApiObject> call, Throwable t) {
                    MainActivity.this.showLoading(false);
                }
            });
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        ShowImageActivity_.intent(this).urlImage(slider.getBundle().get("extra") + "").start();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {}

    @Override
    public void onPageScrollStateChanged(int state) {}
}