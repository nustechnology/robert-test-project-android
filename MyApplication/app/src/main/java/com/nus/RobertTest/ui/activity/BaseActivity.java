package com.nus.RobertTest.ui.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;

import com.nus.RobertTest.utils.LoadingViewUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.greenrobot.eventbus.EventBus;

@EActivity
public class BaseActivity extends AppCompatActivity {

    private LoadingViewUtil mProgressBarUtil;
    /*
*   Make sure all injected stuck are called after this method
* */
    @AfterViews
    void setupActivity() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        init();
    }

    protected void init() {
    }

    protected Activity getActivityContext() {
        return this;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (useEventBus()) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        if (useEventBus()) {
            EventBus.getDefault().unregister(this);
        }

        super.onStop();
    }

    protected boolean useEventBus() {
        return false;
    }

    @UiThread
    void showLoading(boolean isShowing) {
        showLoading(isShowing, true);
    }

    @UiThread
    void showLoading(boolean isShowing, boolean isLockScreen) {
        if (isShowing) {
            if (mProgressBarUtil == null) {
                mProgressBarUtil = new LoadingViewUtil(this);
                mProgressBarUtil.show(isLockScreen);
            } else if (!mProgressBarUtil.isShowing()) {
                mProgressBarUtil.show(isLockScreen);
            }
        } else {
            if (mProgressBarUtil != null) {
                mProgressBarUtil.hide();
            }
        }
    }
}
