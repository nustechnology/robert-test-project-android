package com.nus.RobertTest.services.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AddressHistory {

    @SerializedName("Area")
    @Expose
    private Integer area;
    @SerializedName("Floor")
    @Expose
    private Integer floor;
    @SerializedName("HistoryData")
    @Expose
    private List<HistoryDatum> historyData = new ArrayList<HistoryDatum>();
    @SerializedName("LivingSpace")
    @Expose
    private Integer livingSpace;
    @SerializedName("PropertyTypeID")
    @Expose
    private Integer propertyTypeID;
    @SerializedName("Rooms")
    @Expose
    private Float rooms;
    @SerializedName("SubPropertyTypeID")
    @Expose
    private ApiObject subPropertyTypeID;


    public Integer getArea() {
        return area;
    }

    public Integer getFloor() {
        return floor;
    }

    public List<HistoryDatum> getHistoryData() {
        return historyData;
    }

    public Integer getLivingSpace() {
        return livingSpace;
    }

    public Integer getPropertyTypeID() {
        return propertyTypeID;
    }

    public Float getRooms() {
        return rooms;
    }

    public ApiObject getSubPropertyTypeID() {
        return subPropertyTypeID;
    }

    public String getPropertyTypeString(){
        if (propertyTypeID != null)
            return PropertyType.getString(propertyTypeID);
        else
            return "N/A";
    }
}
