package com.nus.RobertTest.ui.activity;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.nus.RobertTest.R;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import uk.co.senab.photoview.PhotoViewAttacher;

@EActivity(R.layout.activity_show_image)
public class ShowImageActivity extends BaseActivity {

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.image_main)
    ImageView imgMain;

    @Extra
    String urlImage;

    private PhotoViewAttacher mAttacher;

    @Override
    protected void init() {
        super.init();

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);

        if (urlImage != null && urlImage.length() != 0)
            Picasso.with(this).load(urlImage).into(imgMain);

        mAttacher = new PhotoViewAttacher(imgMain);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
