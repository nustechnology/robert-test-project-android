package com.nus.RobertTest.services.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeoCoordinates {

    @SerializedName("Latitude")
    @Expose
    private Float latitude;
    @SerializedName("Longitude")
    @Expose
    private Float longitude;

    public Float getLatitude() {
        return latitude;
    }

    public Float getLongitude() {
        return longitude;
    }
}
