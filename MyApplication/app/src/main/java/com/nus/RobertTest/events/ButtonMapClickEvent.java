package com.nus.RobertTest.events;

import android.widget.ImageButton;

public class ButtonMapClickEvent {
    private ImageButton btn;

    public ButtonMapClickEvent(ImageButton btn) {
        this.btn = btn;
    }

    public ImageButton getBtn() {
        return btn;
    }
}
