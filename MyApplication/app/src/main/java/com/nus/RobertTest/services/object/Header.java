package com.nus.RobertTest.services.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Header {

    @SerializedName("DebugMessage")
    @Expose
    private String debugMessage;
    @SerializedName("IsVerified")
    @Expose
    private ApiObject isVerified;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("RequestKey")
    @Expose
    private Integer requestKey;
    @SerializedName("ResponseType")
    @Expose
    private String responseType;
    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("StatusMessage")
    @Expose
    private String statusMessage;

    public String getDebugMessage() {
        return debugMessage;
    }

    public ApiObject getIsVerified() {
        return isVerified;
    }

    public String getMessage() {
        return message;
    }

    public Integer getRequestKey() {
        return requestKey;
    }

    public String getResponseType() {
        return responseType;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }
}
