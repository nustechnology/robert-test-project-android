package com.nus.RobertTest.services.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ApiObject {

    @SerializedName("AdConfig")
    @Expose
    private Object adConfig;
    @SerializedName("Header")
    @Expose
    private Header header;
    @SerializedName("AdId")
    @Expose
    private Integer adId;
    @SerializedName("AddressHistory")
    @Expose
    private List<AddressHistory> addressHistory = new ArrayList<AddressHistory>();
    @SerializedName("AdvertisementEntries")
    @Expose
    private Object advertisementEntries;
    @SerializedName("Area")
    @Expose
    private Integer area;
    @SerializedName("Availability")
    @Expose
    private Object availability;
    @SerializedName("BuildingArea")
    @Expose
    private Object buildingArea;
    @SerializedName("ChangeDate")
    @Expose
    private String changeDate;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("ComparisPoints")
    @Expose
    private Object comparisPoints;
    @SerializedName("ComparisPrice")
    @Expose
    private Object comparisPrice;
    @SerializedName("ComparisUrl")
    @Expose
    private String comparisUrl;
    @SerializedName("Contact")
    @Expose
    private Contact contact;
    @SerializedName("ContactFormUrl")
    @Expose
    private Object contactFormUrl;
    @SerializedName("ContactSiteID")
    @Expose
    private Integer contactSiteID;
    @SerializedName("ContactSiteName")
    @Expose
    private String contactSiteName;
    @SerializedName("CurrencyID")
    @Expose
    private String currencyID;
    @SerializedName("DealTypeID")
    @Expose
    private Integer dealTypeID;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("DetailImpressionTrackingUrl")
    @Expose
    private Object detailImpressionTrackingUrl;
    @SerializedName("Distances")
    @Expose
    private String distances;
    @SerializedName("ExtraCosts")
    @Expose
    private Integer extraCosts;
    @SerializedName("Features")
    @Expose
    private String features;
    @SerializedName("Floor")
    @Expose
    private Object floor;
    @SerializedName("GeoCoordinates")
    @Expose
    private GeoCoordinates geoCoordinates;
    @SerializedName("ImageCount")
    @Expose
    private Integer imageCount;
    @SerializedName("ImageUrls")
    @Expose
    private List<String> imageUrls = new ArrayList<String>();
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("IsAddressDistinct")
    @Expose
    private Boolean isAddressDistinct;
    @SerializedName("IsStuffleAd")
    @Expose
    private Boolean isStuffleAd;
    @SerializedName("LastRelevantChangeDate")
    @Expose
    private String lastRelevantChangeDate;
    @SerializedName("LivingSpace")
    @Expose
    private Object livingSpace;
    @SerializedName("LocationFactors")
    @Expose
    private LocationFactors locationFactors;
    @SerializedName("OverallPriceChange")
    @Expose
    private Object overallPriceChange;
    @SerializedName("PhoneCallTrackingUrl")
    @Expose
    private Object phoneCallTrackingUrl;
    @SerializedName("Price")
    @Expose
    private Object price;
    @SerializedName("PriceDevelopment")
    @Expose
    private Object priceDevelopment;
    @SerializedName("PriceType")
    @Expose
    private Integer priceType;
    @SerializedName("PriceValue")
    @Expose
    private Integer priceValue;
    @SerializedName("PropertyTypeID")
    @Expose
    private Integer propertyTypeID;
    @SerializedName("RedirectLinks")
    @Expose
    private List<RedirectLink> redirectLinks = new ArrayList<RedirectLink>();
    @SerializedName("ReleaseDate")
    @Expose
    private String releaseDate;
    @SerializedName("RentInclusiveHeating")
    @Expose
    private Integer rentInclusiveHeating;
    @SerializedName("Rooms")
    @Expose
    private Float rooms;
    @SerializedName("SavingPotential")
    @Expose
    private Object savingPotential;
    @SerializedName("SiteID")
    @Expose
    private Integer siteID;
    @SerializedName("SiteName")
    @Expose
    private String siteName;
    @SerializedName("SiteSource")
    @Expose
    private Integer siteSource;
    @SerializedName("StreetName")
    @Expose
    private String streetName;
    @SerializedName("SubPropertyTypeID")
    @Expose
    private Object subPropertyTypeID;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("UsefulArea")
    @Expose
    private Integer usefulArea;
    @SerializedName("YearOfConstruction")
    @Expose
    private Integer yearOfConstruction;
    @SerializedName("Zip")
    @Expose
    private String zip;


    public Object getAdConfig() {
        return adConfig;
    }

    public Header getHeader() {
        return header;
    }

    public Integer getAdId() {
        return adId;
    }

    public List<AddressHistory> getAddressHistory() {
        return addressHistory;
    }

    public Object getAdvertisementEntries() {
        return advertisementEntries;
    }

    public Integer getArea() {
        return area;
    }

    public Object getAvailability() {
        return availability;
    }

    public Object getBuildingArea() {
        return buildingArea;
    }

    public String getChangeDate() {
        return changeDate;
    }

    public String getCity() {
        return city;
    }

    public Object getComparisPoints() {
        return comparisPoints;
    }

    public Object getComparisPrice() {
        return comparisPrice;
    }

    public String getComparisUrl() {
        return comparisUrl;
    }

    public Contact getContact() {
        return contact;
    }

    public Object getContactFormUrl() {
        return contactFormUrl;
    }

    public Integer getContactSiteID() {
        return contactSiteID;
    }

    public String getContactSiteName() {
        return contactSiteName;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public Integer getDealTypeID() {
        return dealTypeID;
    }

    public String getDescription() {
        return description;
    }

    public Object getDetailImpressionTrackingUrl() {
        return detailImpressionTrackingUrl;
    }

    public String getDistances() {
        return distances;
    }

    public Integer getExtraCosts() {
        return extraCosts;
    }

    public String getFeatures() {
        return features;
    }

    public Object getFloor() {
        return floor;
    }

    public GeoCoordinates getGeoCoordinates() {
        return geoCoordinates;
    }

    public Integer getImageCount() {
        return imageCount;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public Boolean getActive() {
        return isActive;
    }

    public Boolean getAddressDistinct() {
        return isAddressDistinct;
    }

    public Boolean getStuffleAd() {
        return isStuffleAd;
    }

    public String getLastRelevantChangeDate() {
        return lastRelevantChangeDate;
    }

    public Object getLivingSpace() {
        return livingSpace;
    }

    public LocationFactors getLocationFactors() {
        return locationFactors;
    }

    public Object getOverallPriceChange() {
        return overallPriceChange;
    }

    public Object getPhoneCallTrackingUrl() {
        return phoneCallTrackingUrl;
    }

    public Object getPrice() {
        return price;
    }

    public Object getPriceDevelopment() {
        return priceDevelopment;
    }

    public Integer getPriceType() {
        return priceType;
    }

    public Integer getPriceValue() {
        return priceValue;
    }

    public Integer getPropertyTypeID() {
        return propertyTypeID;
    }

    public List<RedirectLink> getRedirectLinks() {
        return redirectLinks;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public Integer getRentInclusiveHeating() {
        return rentInclusiveHeating;
    }

    public Float getRooms() {
        return rooms;
    }

    public Object getSavingPotential() {
        return savingPotential;
    }

    public Integer getSiteID() {
        return siteID;
    }

    public String getSiteName() {
        return siteName;
    }

    public Integer getSiteSource() {
        return siteSource;
    }

    public String getStreetName() {
        return streetName;
    }

    public Object getSubPropertyTypeID() {
        return subPropertyTypeID;
    }

    public String getTitle() {
        return title;
    }

    public Integer getUsefulArea() {
        return usefulArea;
    }

    public Integer getYearOfConstruction() {
        return yearOfConstruction;
    }

    public String getZip() {
        return zip;
    }

    public String getPropertyTypeString(){
        if (propertyTypeID != null)
            return PropertyType.getString(propertyTypeID);
        else
            return "N/A";
    }
}

