package com.nus.RobertTest.events;

import android.widget.ImageButton;

public class ButtonStreetMapClickEvent {
    private ImageButton btn;

    public ButtonStreetMapClickEvent(ImageButton btn) {
        this.btn = btn;
    }

    public ImageButton getBtn() {
        return btn;
    }
}
