package com.nus.RobertTest.services.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {

    @SerializedName("ContactComment")
    @Expose
    public ApiObject contactComment;
    @SerializedName("ContactName")
    @Expose
    public String contactName;
    @SerializedName("ContactTel")
    @Expose
    public String contactTel;
    @SerializedName("VendorContact")
    @Expose
    public String vendorContact;

}
