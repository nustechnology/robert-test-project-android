package com.nus.RobertTest.services.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationFactors {

    @SerializedName("Exposition")
    @Expose
    private String exposition;
    @SerializedName("Gradient")
    @Expose
    private String gradient;
    @SerializedName("Height")
    @Expose
    private Integer height;
    @SerializedName("SunshineHoursSummer")
    @Expose
    private Integer sunshineHoursSummer;
    @SerializedName("SunshineHoursWinter")
    @Expose
    private Integer sunshineHoursWinter;

    public String getExposition() {
        return exposition;
    }

    public String getGradient() {
        return gradient;
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getSunshineHoursSummer() {
        return sunshineHoursSummer;
    }

    public Integer getSunshineHoursWinter() {
        return sunshineHoursWinter;
    }
}
