package com.nus.RobertTest.services;

import com.nus.RobertTest.services.object.ApiObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {



//    @GET("json/getdetails?requestObject={\"AdId\":{id}, \"Header\":{\"ApplicationVersion\":null,\"BundleName\":null,\"Device\":null,\"DeviceApplicationGUID\":\"00000000-0000-0000-0000-000000000000\",\"Language\":\"de\",\"Model\":null,\"RequestKey\":0,\"ScreenHeight\":0,\"ScreenWidth\":0}}")
//    Call<ApiObject> testApi(@Path("id") String stringId);

    @GET("json/getdetails")
    Call<ApiObject> testApi(@Query("requestObject") JSONObject json);
}
