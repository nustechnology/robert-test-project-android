package com.nus.RobertTest.services.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RedirectLink {

    @SerializedName("IsClickable")
    @Expose
    private Boolean isClickable;
    @SerializedName("SiteName")
    @Expose
    private String siteName;
    @SerializedName("TrackingUrls")
    @Expose
    private List<String> trackingUrls = new ArrayList<String>();
    @SerializedName("Url")
    @Expose
    private String url;

    public Boolean getClickable() {
        return isClickable;
    }

    public String getSiteName() {
        return siteName;
    }

    public List<String> getTrackingUrls() {
        return trackingUrls;
    }

    public String getUrl() {
        return url;
    }
}
