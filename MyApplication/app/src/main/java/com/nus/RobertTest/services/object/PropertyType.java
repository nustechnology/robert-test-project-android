package com.nus.RobertTest.services.object;

import com.nus.RobertTest.GlobalApplication;
import com.nus.RobertTest.R;

public class PropertyType {

    public static String[] arrayType = GlobalApplication.getInstance().getResources().getStringArray(R.array.array_property);
    public static String getString(int type){

        String result = arrayType[0];
        if (type < arrayType.length){
            result = arrayType[type];
        }
        return result;
    }
}
