package com.nus.RobertTest.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.nus.RobertTest.GlobalApplication;

public class PermissionsUtil {
    public static boolean checkPermissions(String... permissions){
        for (String permission : permissions) {
            if (!checkPermissions(permission)){
                return false;
            }
        }
        return true;
    }

    public static boolean checkPermissions(String permission){
        return ActivityCompat.checkSelfPermission(GlobalApplication.getInstance(), permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermissions(Activity activity, int requestCode, String... permissions){
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }
}
